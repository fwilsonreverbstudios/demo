var movieApp = angular.module('movieApp', []);

movieApp.controller('ListController', function ListController($scope, $http){
  $http.get('components/js/movies.json').success(function(data){
    $scope.movieList = data.movies;
  });
});