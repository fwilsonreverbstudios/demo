# README #

Simple AngularJS site that displays the contents of a JSON file and allows filtering using 2-way data binding.

The demo project utilizes node.js and gulp to stand-up a simple web server to support loading of the JSON file. 

The LESS CSS preprocessor is used to generate the css and minimize it. Processing of the CSS is handled via an IDE plugin but could also be achieved using the gulp-less plugin.

### Installation ###

* Install node.js- https://nodejs.org
* Clone this repository
* Open a command prompt at the location of the cloned project  
* Run "npm install". This will install gulp and gulp connect
* Run "gulp". This will start a simple web server. Copy the url of the server (usually http://localhost:8080) and paste it into your browser.